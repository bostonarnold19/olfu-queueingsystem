@extends('layouts.customer')
@section('title', '| Queue')
@section('content')
<div class="row">
	<div class="col-md-6 bottom-panel-margin">
		<div class="form-top">
			<div class="form-top-left">
				<h3>Now Serving</h3>
			</div>
			<div class="form-top-right">
				<i class="fa fa-spinner fa-spin"></i>
			</div>
		</div> {{-- form-top --}}
		<div class="form-bottom">
			<div class="row">
				<table class="table table-text text-center table-queue">
					<thead class="table-queue-text-thead-custom">
						<tr>
							<th class="text-center" width="300">Counter</th>
							<th class="text-center" >Queue No.</th>
						</tr>
					</thead>
					<tbody class="table-queue-text-custom">
						<?php $count = $queues->count(); ?>
						@foreach($queues as $queue)
						@if($queue->status == 'Serving')
						<tr>
							<td>{{ $queue->user_id-1 }}</td>
							<td>{{ $count }}</td>
						</tr>
						@endif
						<?php $count-- ?>
						@endforeach
					</tbody>
				</table>
			</div>
		</div> {{-- form-bottom --}}
	</div>
	<div class="col-md-6">
		<div class="form-top">
			<div class="form-top-left">
				<h3>Pending Customers</h3>
				<p>Remaining: <i>{{ $queues->where('status', 'Pending')->count() }}</i></p>
			</div>
			<div class="form-top-right">
				<i class="fa fa-hourglass-half fa-spin"></i>
			</div>
		</div> {{-- form-top --}}
		<div class="form-bottom">
			<table class="table table-text">
				<thead>
					<tr>
						<th width="120">Queue No</th>
						<th>Name</th>
					</tr>
				</thead>
				<tbody>
					<?php $count = $queues->count(); ?>
					@foreach($queues as $queue)
					@if($queue->status == 'Pending')
					<tr>
						<td>{{ $count }}</td>
						<td>{{ $queue->name }}</td>
					</tr>
					@endif
					<?php $count-- ?>
					@endforeach
				</tbody>
			</table>
		</div> {{-- form-bottom --}}
	</div>
</div> {{-- row --}}
@endsection