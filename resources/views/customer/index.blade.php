@extends('layouts.customer')
@section('title', '| Registration')
@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="form-top">
            <div class="form-top-left">
                <h3>Register to get a number</h3>
                <p>Fill in the form below:</p>
            </div>
            <div class="form-top-right">
                <i class="fa fa-pencil"></i>
            </div>
        </div> {{-- form-top --}}
        <div class="form-bottom">
            <form action="{{ route('post.index' )}}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <input type="text" class="olfu-custom-input form-control" name="student_number" value="{{ old('student_number') }}" placeholder="Student Number..">
                </div>
                <div class="form-group">
                    <input type="text" required class="olfu-custom-input form-control" name="name" value="{{ old('name') }}" placeholder="Name..">
                </div>
                <div class="form-group">
                    <select class="olfu-custom-input form-control" name="course">
                        <option class="no-display">Course</option>
                        <option value="bsit">BSIT</option>
                        <option value="bstm">BSTM</option>
                        <option value="pharma">PHARMA</option>
                        <option value="medtech">MEDTECH</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" required class="olfu-custom-input form-control" name="inquiry" value="{{ old('inquiry') }}" placeholder="Inquiry..">
                </div>
                <button type="submit" class="olfu-custom-button">Register!</button>
            </form>
        </div> {{-- form-bottom --}}
    </div> {{-- col-md-8 --}}
</div> {{-- row --}}
@endsection
@section('scripts')
<script type="text/javascript">
$(window).load(function()
{
$('#confirmation_message').modal('show');
});
</script>
@endsection