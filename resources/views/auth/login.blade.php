@extends('layouts.cashier')
@section('title', '| Login')
@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="form-top">
            <div class="form-top-left">
                <h3>Login</h3>
                <p>Enter your username and password to log on:</p>
            </div>
            <div class="form-top-right">
                <i class="fa fa-lock"></i>
            </div>
        </div> {{-- form-top --}}
        <div class="form-bottom">
            <form role="form" action="{{ route('post.login') }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <input type="text" required name="username" value="{{ old('username') }}" placeholder="Username.." class="olfu-custom-input form-control">
                </div>
                <div class="form-group">
                    <input required type="password" name="password" placeholder="Password.." class="olfu-custom-input form-control">
                </div>
                <button type="submit" class="olfu-custom-button">Login!</button>
            </form>
        </div> {{-- form-bottom --}}
    </div> {{-- col-md-8 --}}
</div> {{-- row --}}
@endsection
@section('scripts')
<script type="text/javascript">
$(window).load(function()
{
$('#error_message').modal('show');
});
</script>
@endsection