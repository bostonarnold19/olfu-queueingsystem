<!DOCTYPE html>
<html>
    <head>
        @include('partials.cashier._head')
        @yield('style')
    </head>
    <body class="custom-background">
        @include('partials.cashier._nav')
        <div class="container">
            @include('partials.cashier._messages')
            @yield('content')
        </div> {{-- container --}}
        @include('partials.cashier._javascript')
        @yield('scripts')
    </body>
</html>