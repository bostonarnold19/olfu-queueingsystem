<!DOCTYPE html>
<html>
    <head>
        @include('partials.admin._head')
        @yield('style')
    </head>
    <body class="custom-background">
        @include('partials.admin._nav')
        <div class="container">
            @include('partials.admin._messages')
            @yield('content')
        </div> {{-- container --}}
        @include('partials.admin._javascript')
        @yield('scripts')
    </body>
</html>