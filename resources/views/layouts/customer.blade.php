<!DOCTYPE html>
<html>
    <head>
        @include('partials.customer._head')
        @yield('style')
    </head>
    <body class="custom-background">
        @include('partials.customer._nav')
        @include('partials.customer._messages')
        <div class="container-fluid">
            @yield('content')
        </div> {{-- container --}}
        @include('partials.customer._javascript')
        @yield('scripts')
    </body>
</html>