<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<title>OLFU @yield('title')</title>
<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="{{url('https://fonts.googleapis.com/css?family=Roboto:400,100,300,500')}}">
<link rel="stylesheet" href="{{url('https://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{url('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{url('https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css')}}">
{{-- Custom CSS --}}
<link rel="stylesheet" href="{{url('/css/olfu-queue.css')}}">
<style type="text/css">
.table-queue-text-custom {
	font-size: 30px;
}
.table-queue-text-thead-custom {
	font-size: 20px;
}
.table-queue tbody tr td {
	border: none !important;
}
</style>