@if(Session::has('flash_message'))
<div class="modal fade" id="confirmation_message" role="dialog">
    <div class="modal-dialog modal-custom-margin">
        <div class="modal-content modal-custom-color ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Success: {{ Session::get('flash_message') }}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p><span>Queue Number:</span> {{ Session::get('flash_queue') }}</p>
                        <p><span>Name:</span> {{ Session::get('flash_name') }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@if ($errors->any())
<div class="modal fade" id="confirmation_message" role="dialog">
    <div class="modal-dialog modal-custom-margin">
        <div class="modal-content modal-custom-color-error">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Error: 400 Bad Request</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    @foreach ($errors->all() as $error)
                    <div class="col-md-12">
                        <p>{{ $error }}</p>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endif