<nav class="navbar navbar-inverse navbar-fixed-top nav-custom">
    <div class="container">
        <ul class="nav navbar-nav">
            <li>
                <a class="nav-custom-color navbar-brand" href="#">Our Lady of Fatima</a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            @if (Auth::guest())
            <li>
                <a class="nav-custom-color" href="{{ route('get.login') }}">Login</a>
            </li>
            @else
            <li>
                <a class="nav-custom-color" href="{{route('get.cashier')}}">{{ Auth::user()->username }}</a>
            </li>
            <li class="dropdow">
                <a href="#" class="dropdown-toggle nav-custom-color dropdown-toggle-custom" data-toggle="dropdown" role="button" aria-expanded="true">
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-custom" role="menu">
                    <li><a class="dropdown-list-custom" href="{{ route('get.logout') }}">Logout</a></li>
                </ul>
            </li>
            @endif
        </ul>
    </div> {{-- container --}}
</nav> {{-- nav --}}