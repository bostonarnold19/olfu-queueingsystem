@if ($errors->any())
<div class="modal fade" id="error_message" role="dialog">
	<div class="modal-dialog modal-custom-margin">
		<div class="modal-content modal-custom-color-error">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Error: 401 Unauthorized</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					@foreach ($errors->all() as $error)
					<div class="col-md-12">
						<p>{{ $error }}</p>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
@endif