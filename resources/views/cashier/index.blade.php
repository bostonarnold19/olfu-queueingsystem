@extends('layouts.cashier')
@section('title', '| Cashier')
@section('content')
<div class="row">
	<div class="col-md-6 bottom-panel-margin">
		<div class="form-top">
			<div class="form-top-left">
				<h3>Pending Customers</h3>
				<p>Remaining: <i>{{ $pending_queues->where('status', 'Pending')->count() }}</i></p>
			</div>
			<div class="form-top-right">
				<i class="fa fa-hourglass-end"></i>
			</div>
		</div> {{-- form-top --}}
		<div class="form-bottom">
			<table class="table table-text table-custom">
				<thead>
					<tr>
						<th width="120">Queue No</th>
						<th>Name</th>
					</tr>
				</thead>
				<tbody>
					<?php $count = $pending_queues->count(); ?>
					@foreach($pending_queues as $queue)
					@if($queue->status == 'Pending')
					<tr>
						<td>{{ $count }}</td>
						<td>{{ $queue->name }}</td>
					</tr>
					@endif
					<?php $count-- ?>
					@endforeach
				</tbody>
			</table>
			<div class="form-group">
				<form action="{{ route('post.serve') }}" method="post">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="id" value="{{ !empty($pending_first->id) ? $pending_first->id : '' }}">
					<button {{ !empty($serving_queue->where('status', 'Serving')->first()) || empty($pending_queues->where('status', 'Pending')->first()) ? 'disabled' : '' }} type="submit" class="olfu-custom-button">Serve</button>
				</form>
			</div>
		</div> {{-- form-bottom --}}
	</div>
	<div class="col-md-6">
		<div class="form-top">
			<div class="form-top-left">
				<h3>Now Serving</h3>
				<p>Queue Number:
					<?php $count = $pending_queues->count(); ?>
					@foreach($pending_queues as $queue)
					@if($queue->status == 'Serving' && $queue->user_id == Auth::id())
					<i>{{ !empty($serving_queue->where('status', 'Serving')->first()) ? $count : 'XXX' }}</i>
					@endif
					<?php $count-- ?>
					@endforeach
				</p>
			</div>
			<div class="form-top-right">
				<i class="fa fa-spinner fa-spin"></i>
			</div>
		</div> {{-- form-top --}}
		<div class="form-bottom">
			<hr>
			<div class="row">
				<div class="form-group">
					<p class="col-md-3 col-sm-3 serving-text-color">Student No: </p>
					<div class="col-md-9 col-sm-9">
						<p class="info-text">
							{{ !empty($serving_queue->where('status', 'Serving')->first()) ? $serving_queue->where('status', 'Serving')->first()->student_number : 'XXX' }}
						</p>
					</div>
				</div>
				<div class="form-group">
					<p class="col-md-3 col-sm-3 serving-text-color">Name: </p>
					<div class="col-md-9 col-sm-9">
						<p class="info-text">
							{{ !empty($serving_queue->where('status', 'Serving')->first()) ? $serving_queue->where('status', 'Serving')->first()->name : 'XXX' }}
						</p>
					</div>
				</div>
				<div class="form-group">
					<p class="col-md-3 col-sm-3 serving-text-color">Course: </p>
					<div class="col-md-9 col-sm-9">
						<p class="info-text">
							{{ !empty($serving_queue->where('status', 'Serving')->first()) ? $serving_queue->where('status', 'Serving')->first()->course : 'XXX' }}
						</p>
					</div>
				</div>
				<div class="form-group">
					<p class="col-md-3 col-sm-3 serving-text-color">Inquiry: </p>
					<div class="col-md-9 col-sm-9">
						<p class="info-text">
							{{ !empty($serving_queue->where('status', 'Serving')->first()) ? $serving_queue->where('status', 'Serving')->first()->inquiry : 'XXX' }}
						</p>
					</div>
				</div>
				<div class="form-group">
					<form action="{{ route('post.done') }}" method="post">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id" value="{{ !empty($serving_queue->where('status', 'Serving')->first()) ? $serving_queue->where('status', 'Serving')->first()->id : '' }}">
						<button {{ !empty($serving_queue->where('status', 'Serving')->first()) ? '' : 'disabled' }} type="submit" class="olfu-custom-button disabled">Done</button>
					</form>
				</div>
				<div class="form-group">
					<form action="{{ route('post.skip') }}" method="post">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id" value="{{ !empty($serving_queue->where('status', 'Serving')->first()) ? $serving_queue->where('status', 'Serving')->first()->id : '' }}">
						<button {{ !empty($serving_queue->where('status', 'Serving')->first()) ? '' : 'disabled' }} type="submit" class="olfu-custom-button">Skip</button>
					</form>
				</div>
			</div>
		</div> {{-- form-bottom --}}
	</div>
</div> {{-- row --}}
@stop