@extends('layouts.admin')
@section('title', '| Cashier')
@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="form-top">
            <div class="form-top-left">
                <h3>Accounts Management</h3>
                <p>Number of accounts: {{ App\User::all()->count()-1}}</p>
                <a href="{{ route('get.account.register') }}" class="href-text">Register new account?</a>
            </div>
            <div class="form-top-right">
                <i class="fa fa-user"></i>
            </div>
        </div> {{-- form-top --}}
        <div class="form-bottom">
            <table class="table table-text table-custom">
                <thead>
                    <tr>
                        <th class="text-center" width="50">ID</th>
                        <th width="50">Username</th>
                        <th class="text-center" width="50">Cashier</th>
                        <th class="text-center" width="50">Admin</th>
                        <th class="text-center" width="50">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <form action="{{ route('assign.account.access') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $user->id }}"></td>
                            <td class="text-center">{{ $user->id }}</td>
                            <td>{{ $user->username }}</td>
                            <td><input type="checkbox" class="form-control" {{ $user->hasRole('Cashier') ? 'checked' : '' }} name="role_cashier"></td>
                            <td><input type="checkbox" class="form-control" {{ $user->hasRole('Admin') ? 'checked' : '' }} name="role_admin"></td>
                            <td class="text-center"><input type="submit" class="btn custom-button-assign" value="Assign Role"></td>
                        </form>
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>
        </div> {{-- form-bottom --}}
    </div> {{-- col-md-12 --}}
</div> {{-- row --}}
@stop