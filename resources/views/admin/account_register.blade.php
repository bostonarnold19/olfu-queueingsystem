@extends('layouts.admin')
@section('title', '| Account Register')
@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="form-top">
            <div class="form-top-left">
                <h3>Register Account</h3>
                <p>Fill in the form below:</p>
            </div>
            <div class="form-top-right">
                <i class="fa fa-pencil"></i>
            </div>
        </div> {{-- form-top --}}
        <div class="form-bottom">
            <form action="{{ route('post.account.register') }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <input type="text" required class="olfu-custom-input form-control" name="username" value="{{ old('username') }}" placeholder="Username..">
                </div>
                <div class="form-group">
                    <input type="password" required class="olfu-custom-input form-control" name="password" value="{{ old('password') }}" placeholder="Password..">
                </div>
                <div class="form-group">
                    <input type="password" required class="olfu-custom-input form-control" name="password_confirmation" value="{{ old('password_confirmation') }}" placeholder="Password Confirmation..">
                </div>
                <button type="submit" class="olfu-custom-button">Register!</button>
            </form>
        </div> {{-- form-bottom --}}
    </div> {{-- col-md-8 --}}
</div> {{-- row --}}
@endsection
@section('scripts')
<script type="text/javascript">
$(window).load(function()
{
$('#confirmation_message').modal('show');
});
</script>
@endsection