<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Queue;
use App\User;
use Carbon\Carbon;
use Auth;

class CashierController extends Controller
{
	
   public function getCashier()
	{
		$auth = Auth::user();

		$pending_queues = Queue::whereDate('created_at', Carbon::now()->toDateString())->get()->sortByDesc('id');
        $serving_queue = Queue::whereDate('created_at', Carbon::now()->toDateString())->where('user_id', $auth->id)->get();
        $pending_first = Queue::whereDate('created_at', Carbon::now()->toDateString())->where('status', 'Pending')->first();

		return view('cashier.index', compact('pending_queues', 'serving_queue', 'pending_first'));
	}

	public function postServe(Request $request)
	{
		$auth = Auth::user();

		if(!empty($request->id)) {
			$serving_queue = Queue::find($request->id);
			$serving_queue->user_id = $auth->id;
			$serving_queue->status = 'Serving';
	      	$serving_queue->update();
      		return redirect()->route('get.cashier');
      	} else {
      		return redirect()->route('get.cashier');
      	}

  		return redirect()->route('get.cashier');

	}

	public function postSkip(Request $request)
	{
		if(!empty($request->id)) {
			$skip_queue = Queue::find($request->id);
			$skip_queue->status = 'Skip';
	      	$skip_queue->update();
      		return redirect()->route('get.cashier');
      	} else {
      		return redirect()->route('get.cashier');
      	}

      	return redirect()->route('get.cashier');
	}

	public function postDone(Request $request)
	{
		$done_queue = Queue::find($request->id);
		$done_queue->status = 'Done';
      	$done_queue->update();

      	return redirect()->route('get.cashier');
	}
}