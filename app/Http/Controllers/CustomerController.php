<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\CustomerRequest;
use App\Queue;
use App\User;
use Session;
use Carbon\Carbon;

class CustomerController extends Controller
{

    public function getIndex()
    {
    	return view('customer.index');
    }

    public function postIndex(CustomerRequest $request)
    {
        $pending_queues = Queue::whereDate('created_at', Carbon::now()->toDateString())->get();

    	$queue = new Queue();
    	$queue->user_id = 1;
    	$queue->name = ucwords($request->name);
    	$queue->student_number = $request->student_number;
    	$queue->course = $request->course;
    	$queue->inquiry = ucwords($request->inquiry);
        $queue->status = 'Pending';
    	$queue->save();

        Session::flash('flash_message', 'Please wait for your number to be called');
        Session::flash('flash_name', $queue->name);
        Session::flash('flash_queue', $pending_queues->count()+1);
        return redirect()->route('get.index');
    }

    public function getQueue()
    {
        $queues = Queue::whereDate('created_at', Carbon::now()->toDateString())->get()->sortByDesc('id');

    	return view('customer.queue', compact('queues'));
    }

}