<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\RegisterRequest;
use App\User;
use App\Role;
use Session;
use Redirect;
use Auth;

class AdminController extends Controller
{

   public function getDashboard()
   {
   	return view('admin.dashboard');
   }

   public function getAccountRegister()
   {
      return view('admin.account_register');
   }

   public function postAccountRegister(RegisterRequest $request)
   {
   	$user = new User();
      $user->username = $request->username;
      $user->password = bcrypt($request->password);
      $user->save();

      $user->roles()->attach(Role::where('name', 'Cashier')->first());
      
      Session::flash('flash_message', 'The account have been added.');
      return Redirect::route('manage.account.access');
   }

   public function manageAccountAccess()
   {
      $users = User::all()->except(1)->except(Auth::id());
      return view('admin.account_access', compact('users'));
   }

   public function assignAccountAccess(Request $request)
   {
      $user = User::where('id', $request['id'])->firstorfail();
      $user->roles()->detach();
      if ($request['role_cashier']) {
         $user->roles()->attach(Role::where('name', 'Cashier')->firstorfail());
      }
      if ($request['role_admin']) {
         $user->roles()->attach(Role::where('name', 'Admin')->firstorfail());
      }
      Session::flash('flash_message', 'The changes have been saved.');
      return Redirect::route('manage.account.access');
   }

}
