<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Queue extends Model
{

    public function status()
    {
        return $this->belongsToMany('App\Status')
                    ->withTimestamps();
    }

    public function users()
    {
    	return $this->belongsToMany('App\Queue')
    				->withTimestamps();
    }

}
