<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::where('name', 'Admin')->first();
        $role_cashier = Role::where('name', 'Cashier')->first();

        $admin = new User();
        $admin->username = 'admin';
        $admin->password = bcrypt('admin');
        $admin->save();
        $admin->roles()->attach($role_admin);

        $cashier1 = new User();
        $cashier1->username = 'c1';
        $cashier1->password = bcrypt('c1');
        $cashier1->save();
        $cashier1->roles()->attach($role_cashier);

        $cashier2 = new User();
        $cashier2->username = 'c2';
        $cashier2->password = bcrypt('c2');
        $cashier2->save();
        $cashier2->roles()->attach($role_cashier);

        $cashier3 = new User();
        $cashier3->username = 'c3';
        $cashier3->password = bcrypt('c3');
        $cashier3->save();
        $cashier3->roles()->attach($role_cashier);

    }
}