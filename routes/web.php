<?php

Route::get('auth/login', ['uses' => 'Auth\LoginController@getLogin', 'as' => 'get.login']);
Route::post('auth/login', ['uses' => 'Auth\LoginController@postLogin', 'as' => 'post.login']);
Route::get('auth/logout', ['uses' => 'Auth\LoginController@getLogout','as' => 'get.logout']);

Route::get('/', ['uses' => 'CustomerController@getIndex','as' => 'get.index']);
Route::post('/', ['uses' => 'CustomerController@postIndex','as' => 'post.index']);

Route::get('queue', ['uses' => 'CustomerController@getQueue','as' => 'get.queue']);

Route::get('cashier', [
    'uses' => 'CashierController@getCashier',
    'as' => 'get.cashier',
    'middleware' => 'roles',
    'roles' => ['Cashier']
]);

Route::post('cashier/serve', [
    'uses' => 'CashierController@postServe',
    'as' => 'post.serve',
    'middleware' => 'roles',
    'roles' => ['Cashier']
]);

Route::post('cashier/done', [
    'uses' => 'CashierController@postDone',
    'as' => 'post.done',
    'middleware' => 'roles',
    'roles' => ['Cashier']
]);

Route::post('cashier/skip', [
    'uses' => 'CashierController@postSkip',
    'as' => 'post.skip',
    'middleware' => 'roles',
    'roles' => ['Cashier']
]);

Route::get('dashboard', [
    'uses' => 'AdminController@getDashboard',
    'as' => 'get.dashboard',
    'middleware' => 'roles',
    'roles' => ['Admin']
]);

Route::get('manage/account/access', [
    'uses' => 'AdminController@manageAccountAccess',
    'as' => 'manage.account.access',
    'middleware' => 'roles',
    'roles' => ['Admin']
]);

Route::post('assign/account/access', [
    'uses' => 'AdminController@assignAccountAccess',
    'as' => 'assign.account.access',
    'middleware' => 'roles',
    'roles' => ['Admin']
]);

Route::get('account/register', [
    'uses' => 'AdminController@getAccountRegister',
    'as' => 'get.account.register',
    'middleware' => 'roles',
    'roles' => ['Admin']
]);

Route::post('account/register', [
    'uses' => 'AdminController@postAccountRegister',
    'as' => 'post.account.register',
    'middleware' => 'roles',
    'roles' => ['Admin']
]);